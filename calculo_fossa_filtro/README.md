Descrição:  
Esse programa realiza cálculo de fossa de acordo com a NBR 7229 e dimensionamento de filtros anaeróbicos de acordo com a NBR 13969  
Autor: Vitor Brandão   
E-mail: <vitorbrandao.ti@outlook.com>  
Data: 2022-09-27   
Versão: 0.1  
“Quem combate monstruosidades deve cuidar para que não se torne um monstro.  
E se olhares durante muito tempo para um abismo, o abismo também olha para dentro de você.”  
-Friedrich Nietzsche, Além do Bem e do Mal, Prelúdio a uma Filosofia do Futuro (1886)  
